# Scooter Search Microservice

The Scooter Search Microservice provides APIs to handle all scooter search-related tasks. It can be used to:

- Search for scooters within the given radius and coordinates
- More coming in the future!

## Swagger Specs

The swagger specs can be accessed via ```http://{host}:{port}/swagger-ui/```

## Running the Service

There are 2 ways to run the app:

(A) Executing the .jar file inside the `target` folder. Requires Java to be installed.
- To run the .jar file, open up a terminal window and execute the following:
- Navigate to `http://localhost:8080/swagger-ui/` to access the Swagger specs.
```
java -jar demo-0.0.1-SNAPSHOT.jar
```
(B) Building and running the Docker image - does not require Java to be installed, but requires Docker to be available on the machine.
- To run the Docker option, first navigate to the root directory of the project and build the image.
```
docker build -t scooter-tracker-ms .
```
- Once the image has been successfully built, run the image:
```
docker run -p 8080:8080 scooter-tracker-ms
```
- Navigate to http://localhost:8080/swagger-ui/ to access the Swagger specs.



## Developer Guide

The microservice is written using [Java Spring](https://spring.io/). The project structure is largely based on the default structure of a downloaded Spring Boot starter pack [Spring Initializr](https://start.spring.io/). 

```
Build Tool - Maven
Spring Boot Version - 2.3.3
Java Version - 11
```

## Project Structure

There are two main folders in `src` of the project - `main` and `test`. All execution codes are stored in `/main/java`, all properties in `/resources`. Unit tests are created in `test`. There is a main package for the project, each sub-package is created as follows:

| SubPackage | Description | 
| ---------- | ----------- |
| config | All configuration beans should be created in this folder |
| controller | REST controllers should reside here. |
| dao | Holds all data access objects | 
| entity | Holds all entity objects, which are linked to database schema as well | 
| model | Holds all model objects, which should be used between controller and service, and in responses to the external caller. |
| service | All service interfaces and implementation should reside here. |


## Unit Tests

Jacoco has been enabled for tests. After running `mvn test` please check `target/site/jacoco/index.html` for coverage results.


## Dependencies

| Dependency | ArtifactID | Usage |
| ---------- | ---------- | ----- |
| Spring Data JPA | spring-boot-starter-data-jpa | Starter for using Spring Data JPA with Hibernate | 
| Spring Data REST | spring-boot-starter-data-rest | Starter for exposing Spring Data repositories over REST using Spring Data REST |
| Spring Boot Web Starter | spring-boot-starter-web | Starter for building web, including RESTful, applications using Spring MVC. Uses Tomcat as the default embedded container |
| Springfox Boot Starter | springfox-boot-starter | Swagger configuration pack |
| H2 Database | h2 | H2 integrated database engine. No installation required. | 
| Lombok | lombok | Supports the auto-creation of getters, setters and constructors for POJOs. |
| Validation | validation-api | Validates submitted request objects. |
| Model Mapper | modelmapper | Maps fields between objects - reduces overhead in codes. |
| Spring Test | spring-boot-starter-test | Starter for testing Spring Boot applications with libraries including JUnit, Hamcrest and Mockito |


## Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
