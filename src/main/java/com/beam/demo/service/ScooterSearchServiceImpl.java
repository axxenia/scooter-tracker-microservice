package com.beam.demo.service;

import com.beam.demo.dao.ScooterStatusRepository;
import com.beam.demo.entity.ScooterStatusEntity;
import com.beam.demo.model.ScooterStatusResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/** --------------------------------------------------------------------------------------------------------------------
 * The service implementation of the ScooterSearchService interface.
 * Contains all logic used for retrieval of scooters.
 ---------------------------------------------------------------------------------------------------------------------*/
@Service
public class ScooterSearchServiceImpl implements ScooterSearchService{

    @Autowired
    private ScooterStatusRepository scooterStatusRepository;

    // The radius of the earth
    private final int R = 6371;


    @Override
    public List<ScooterStatusResponse> getScootersByLatLon(int numOfScooters,
                                                           double radius,
                                                           double latitude,
                                                           double longitude)  {


        List<ScooterStatusEntity> scooterStatusEntities = scooterStatusRepository.findAll();
        Map<Double, List<ScooterStatusEntity>> selectedScooterEntities = new HashMap<>();


        for(ScooterStatusEntity statusEntity : scooterStatusEntities) {

            // 1. Get lat, lon for each entity
            double scooterLatitude = statusEntity.getLatitude();
            double scooterLongitude = statusEntity.getLongitude();

            // 2. Compare distance between entity and query params using the Haversine Formula
            double latitudeDiff = Math.toRadians(latitude - scooterLatitude);
            double longitudeDiff = Math.toRadians(longitude - scooterLongitude);

            double a = Math.sin(latitudeDiff / 2) * Math.sin(latitudeDiff / 2)
                    + Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(scooterLatitude))
                    * Math.sin(longitudeDiff / 2) * Math.sin(longitudeDiff / 2);

            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double distance = R * c * 1000;

            // 3. Shortlist scooters within the given radius
            if(distance <= radius) {
                List<ScooterStatusEntity> scooterList = new ArrayList<>();

                if (selectedScooterEntities.containsKey(distance)) {
                   scooterList = selectedScooterEntities.get(distance);
                }
                scooterList.add(statusEntity);
                selectedScooterEntities.put(distance, scooterList);
            }
        }

        // 4. Sort scooters by distance
        List<Double> sortedKeys=new ArrayList<>(selectedScooterEntities.keySet());
        Collections.sort(sortedKeys);

        // 5. Return list of scooters. If X is non-zero, select top X
        List<ScooterStatusResponse> resultsList = new ArrayList<>();
        int selectCount = 1;
        ModelMapper modelMapper = new ModelMapper();

        for(Double key : sortedKeys) {

            List<ScooterStatusEntity> getDistanceGroup = selectedScooterEntities.get(key);
            for(ScooterStatusEntity groupEntity : getDistanceGroup) {

                if(numOfScooters > 0 && selectCount > numOfScooters) {
                    break;
                }
                ScooterStatusResponse scooterStatusResponse = modelMapper.map(groupEntity, ScooterStatusResponse.class);
                resultsList.add(scooterStatusResponse);

                ++selectCount;
            }
        }

        return resultsList;
    }
}
