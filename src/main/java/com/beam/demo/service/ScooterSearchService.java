package com.beam.demo.service;

import com.beam.demo.model.ScooterStatusResponse;
import java.util.List;

/** --------------------------------------------------------------------------------------------------------------------
 * The Interface object for scooter search service.
 ---------------------------------------------------------------------------------------------------------------------*/
public interface ScooterSearchService {

    List<ScooterStatusResponse> getScootersByLatLon(int numOfScooters, double radius, double latitude, double longitude);
}
