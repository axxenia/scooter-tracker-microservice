package com.beam.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/** --------------------------------------------------------------------------------------------------------------------
 * The response object used between controller and service.
 * Lombok has been used to initialize no-args and all-args constructors, as well as all setter/getter methods.
 ---------------------------------------------------------------------------------------------------------------------*/

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ScooterStatusResponse {

    private double latitude;

    private double longitude;

    private String status;

    private int usedTimes;
}
