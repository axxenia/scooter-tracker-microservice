package com.beam.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** --------------------------------------------------------------------------------------------------------------------
 * The request object used between controller and service.
 * Lombok has been used to initialize no-args and all-args constructors, as well as all setter/getter methods.
 ---------------------------------------------------------------------------------------------------------------------*/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScooterStatusRequest {

    private int numOfScooters;

    private double radius;

    private double latitude;

    private double longitude;
}
