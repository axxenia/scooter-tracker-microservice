package com.beam.demo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


/** --------------------------------------------------------------------------------------------------------------------
 * The entity object used for accessing database values.
 * Lombok has been used to initialize no-args and all-args constructors, as well as all setter/getter methods.
 ---------------------------------------------------------------------------------------------------------------------*/

@Entity
@Table(name="scooter_status")
@ApiModel(description = "Location and Status of The Scooter")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ScooterStatusEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    @NotNull
    @ApiModelProperty(notes = "Database Generated ID", hidden = true)
    private int id;

    @Column(name="latitude")
    @ApiModelProperty(notes = "Scooter Coordinate Value - Latitude")
    private double latitude;

    @Column(name="longitude")
    @ApiModelProperty(notes = "Scooter Coordinate Value - Longitude")
    private double longitude;

    @Column(name="status")
    @ApiModelProperty(notes = "Scooter Battery Status")
    private String status;

    @Column(name="used")
    @ApiModelProperty(notes = "Times The Scooter Has Been Used")
    private int usedTimes;
}
