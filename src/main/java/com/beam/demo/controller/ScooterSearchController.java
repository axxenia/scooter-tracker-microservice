package com.beam.demo.controller;


import com.beam.demo.model.ScooterStatusRequest;
import com.beam.demo.model.ScooterStatusResponse;
import com.beam.demo.service.ScooterSearchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@Api(value = "Scooter Search Microservice")
@CrossOrigin
public class ScooterSearchController {

    @Autowired
    private ScooterSearchService scooterSearchService;


    /** --------------------------------------------------------------------------------------------------------------
     * POST API for scooters search. The request object contains values for the number of scooters, the radius of
     * the search, the latitude and longitude coordinates.
     *
     * @param searchRequest    The request object for requesting a seerch for scooters
     * @return                 A list of scooters within the given search parameters.
     -------------------------------------------------------------------------------------------------------------- */
    @ApiOperation(value = "Search for scooters with the given params")
    @PostMapping(value = "/scooters")
    public ResponseEntity<Object> searchScooters(HttpServletRequest request,
                                                 @Valid @RequestBody ScooterStatusRequest searchRequest) {

        List<ScooterStatusResponse> searchResponse = scooterSearchService.getScootersByLatLon(searchRequest.getNumOfScooters(),
                searchRequest.getRadius(), searchRequest.getLatitude(), searchRequest.getLongitude());

        if(searchResponse.isEmpty())
            return new ResponseEntity<>(new ArrayList<>(), HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(searchResponse, HttpStatus.OK);
    }
}
