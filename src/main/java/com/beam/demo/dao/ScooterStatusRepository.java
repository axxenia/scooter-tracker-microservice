package com.beam.demo.dao;

import com.beam.demo.entity.ScooterStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


/** -------------------------------------------------------------------------------------------------------------
 * The Data Access Object for scooters entries in the database.
 -------------------------------------------------------------------------------------------------------------- */
public interface ScooterStatusRepository extends JpaRepository<ScooterStatusEntity, Integer> {

    List<ScooterStatusEntity> findAllByStatus(String status);
}
