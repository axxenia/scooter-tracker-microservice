package com.beam.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


@Configuration
public class Swagger2Config {

    @Value("${swagger.title}")
    private String swaggerTitle;

    @Value("${swagger.description}")
    private String swaggerDescription;

    @Value("${swagger.version}")
    private String swaggerVersion;

    @Value("${swagger.basePackage}")
    private String basePackage;

    /** -------------------------------------------------------------------------------------------------------------
     * Initialise Swagger setup
     -------------------------------------------------------------------------------------------------------------- */
    @Bean
    public Docket swagger() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.regex("/.*"))
                .build()
                .apiInfo(
                        new ApiInfoBuilder().title(swaggerTitle)
                                .description(swaggerDescription)
                                .version(swaggerVersion)
                                .build()
                );
    }
}
