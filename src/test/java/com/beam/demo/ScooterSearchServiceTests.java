package com.beam.demo;

import com.beam.demo.dao.ScooterStatusRepository;
import com.beam.demo.entity.ScooterStatusEntity;
import com.beam.demo.model.ScooterStatusResponse;
import com.beam.demo.service.ScooterSearchServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.*;
import static org.assertj.core.api.Assertions.assertThat;



@ExtendWith(MockitoExtension.class)
public class ScooterSearchServiceTests {


    @Mock
    private ScooterStatusRepository scooterStatusRepository;

    @InjectMocks
    private ScooterSearchServiceImpl scooterSearchServiceImpl;


    /** -------------------------------------------------------------------------------------------------------------
     * Test for searching for scooters.
     -------------------------------------------------------------------------------------------------------------- */
    @Test
    public void searchScootersTest() {

        List<ScooterStatusEntity> scooterStatusEntities = new ArrayList<>();
        ScooterStatusEntity entity1 =
                new ScooterStatusEntity(1, 1.29431, 103.791, "OK", 1);
        ScooterStatusEntity entity2 =
                new ScooterStatusEntity(2, 1.275023, 103.845604, "OK", 1);
        ScooterStatusEntity entity3 =
                new ScooterStatusEntity(3, 1.275025, 103.8457, "OK", 1);
        ScooterStatusEntity entity4 =
                new ScooterStatusEntity(4, 1.275025, 103.8457, "OK", 1);
        scooterStatusEntities.add(entity1);
        scooterStatusEntities.add(entity2);
        scooterStatusEntities.add(entity3);
        scooterStatusEntities.add(entity4);

        when(scooterStatusRepository.findAll()).thenReturn(scooterStatusEntities);

        List<ScooterStatusResponse> responses =
                scooterSearchServiceImpl.getScootersByLatLon(2, 500, 1.275025, 103.8457);

        assertThat(responses.size()).isEqualTo(2);
    }
}
