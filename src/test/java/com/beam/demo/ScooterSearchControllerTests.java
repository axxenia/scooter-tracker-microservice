package com.beam.demo;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.beam.demo.controller.ScooterSearchController;
import com.beam.demo.model.ScooterStatusRequest;
import com.beam.demo.model.ScooterStatusResponse;
import com.beam.demo.service.ScooterSearchService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(SpringExtension.class)
@WebMvcTest(ScooterSearchController.class)
public class ScooterSearchControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ScooterSearchService scooterSearchService;


    /** -------------------------------------------------------------------------------------------------------------
     * Test for successful search of scooters.
     -------------------------------------------------------------------------------------------------------------- */
    @Test
    public void searchScooterSuccessTest() throws Exception {

        ScooterStatusRequest request =
                new ScooterStatusRequest(2, 100, 1.295873, 103.783838);
        List<ScooterStatusResponse> searchList = new ArrayList<>();
        searchList.add(new ScooterStatusResponse(1.29431, 103.791, "OK", 1));

        when(scooterSearchService.getScootersByLatLon(any(Integer.class), any(Double.class), any(Double.class), any(Double.class)))
                .thenReturn(searchList);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/scooters")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(request)))
                .andExpect(status().isOk());
    }

    /** -------------------------------------------------------------------------------------------------------------
     * Test for when scooters are not found.
     -------------------------------------------------------------------------------------------------------------- */
    @Test
    public void searchScooterNotFoundTest() throws Exception {

        ScooterStatusRequest request =
                new ScooterStatusRequest(2, 100, 1.295873, 103.783838);
        List<ScooterStatusResponse> searchList = new ArrayList<>();

        when(scooterSearchService.getScootersByLatLon(any(Integer.class), any(Double.class), any(Double.class), any(Double.class)))
                .thenReturn(searchList);

        mockMvc.perform(MockMvcRequestBuilders
                .post("/scooters")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(request)))
                .andExpect(status().isNotFound());
    }



    /** -------------------------------------------------------------------------------------------------------------
     * Helper method for converting objects into JSON.
     -------------------------------------------------------------------------------------------------------------- */
    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
