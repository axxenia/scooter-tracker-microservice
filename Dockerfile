# For best practices on dockerizing a Spring Boot app, check:
# https://spring.io/guides/gs/spring-boot-docker/

FROM openjdk:11

ARG JAR_FILE=/target/demo-0.0.1-SNAPSHOT.jar

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
